package com.practicas.metaEnlace.CitasMedico.services.interfaces;

import com.practicas.metaEnlace.CitasMedico.dto.CitaDTO;
import com.practicas.metaEnlace.CitasMedico.dto.CitaListDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Cita;

import java.util.List;

public interface ICitaService {
    public List<CitaListDTO> list();
    public Cita insert(CitaDTO citaDTO);
    public Cita update(Long id, CitaDTO citaDTO);
    public CitaListDTO search(long id);
    public List<CitaListDTO> searchByPacienteId(long id);
    public void delete(long id);
}
