package com.practicas.metaEnlace.CitasMedico.services.interfaces;

import com.practicas.metaEnlace.CitasMedico.dto.PacienteDTO;
import com.practicas.metaEnlace.CitasMedico.dto.PacienteListDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Paciente;

import java.util.List;

public interface IPacienteService {
    public List<PacienteListDTO> list();
    public Paciente insert(PacienteDTO pacienteDTO);
    public Paciente update(Long id, PacienteDTO pacienteDTO);
    public PacienteDTO searchId(Long id);
    public PacienteDTO search(String usuario);
    public void delete(Long id);

}
