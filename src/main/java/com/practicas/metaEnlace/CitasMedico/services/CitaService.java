package com.practicas.metaEnlace.CitasMedico.services;

import com.practicas.metaEnlace.CitasMedico.dto.CitaDTO;
import com.practicas.metaEnlace.CitasMedico.dto.CitaListDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Cita;
import com.practicas.metaEnlace.CitasMedico.entities.Medico;
import com.practicas.metaEnlace.CitasMedico.entities.Paciente;
import com.practicas.metaEnlace.CitasMedico.exceptions.DataNotFoundException;
import com.practicas.metaEnlace.CitasMedico.mapper.CitaMapper;
import com.practicas.metaEnlace.CitasMedico.repositories.CitaRepository;
import com.practicas.metaEnlace.CitasMedico.repositories.MedicoRepository;
import com.practicas.metaEnlace.CitasMedico.repositories.PacienteRepository;
import com.practicas.metaEnlace.CitasMedico.services.interfaces.ICitaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CitaService implements ICitaService {
    @Autowired
    private CitaRepository citaRepository;
    @Autowired
    private CitaMapper citaMapper;
    @Autowired
    private PacienteRepository pacienteRepository;
    @Autowired
    private MedicoRepository medicoRepository;

    public Cita insert(CitaDTO citaDTO){
        try{
            Paciente paciente = pacienteRepository.findById(citaDTO.getPacienteId()).orElse(null);
            if(paciente == null){
                throw new DataNotFoundException("Paciente no encontrado");
            }
            Medico medico = medicoRepository.findById(citaDTO.getMedicoId()).orElse(null);
            if(medico == null){
                throw new DataNotFoundException("Medico no encontrado");
            }

            Cita cita = citaMapper.toCita(citaDTO);
            cita.setPaciente(paciente);
            cita.setMedico(medico);

            paciente.addCita(cita);
            paciente.addMedico(medico);
            pacienteRepository.save(paciente);

            medico.addCita(cita);
            //medico.addPaciente(paciente);
            medicoRepository.save(medico);

            return citaRepository.save(cita);
        }catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public Cita update(Long id, CitaDTO citaDTO){
        try{
            Paciente paciente = pacienteRepository.findById(citaDTO.getPacienteId()).orElse(null);
            if(paciente == null){
                throw new DataNotFoundException("Paciente no encontrado");
            }
            Medico medico = medicoRepository.findById(citaDTO.getMedicoId()).orElse(null);
            if(medico == null){
                throw new DataNotFoundException("Médico no encontrado");
            }

            Cita cita = citaMapper.toCita(citaDTO);
            cita.setPaciente(paciente);
            cita.setMedico(medico);
            cita.setId(id);

            paciente.addCita(cita);
            paciente.addMedico(medico);
            pacienteRepository.save(paciente);

            medico.addCita(cita);
            medicoRepository.save(medico);

            return citaRepository.save(cita);
        }catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public List<CitaListDTO> list(){
        List<CitaListDTO> lista = new ArrayList<>();
        Iterable<Cita> listaCitas = citaRepository.findAll();
        for(Cita cita : listaCitas){
            CitaListDTO CitaListDTO = citaMapper.toCitaListDTO(cita);
            lista.add(CitaListDTO);
        }
        return lista;
    }

    public CitaListDTO search(long id){
        try{
            Cita cita = citaRepository.findById(id).orElse(null);
            if(cita == null){
                throw new DataNotFoundException("Cita no existe");
            }
            return citaMapper.toCitaListDTO(cita);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public List<CitaListDTO> searchByPacienteId(long id){
        try{
            List<CitaListDTO> citas = new ArrayList<>();
            Iterable<Cita> listaCitas = citaRepository.findByPacienteId(id);
            for(Cita cita : listaCitas){
                CitaListDTO citaListDTO = citaMapper.toCitaListDTO(cita);
                citas.add(citaListDTO);
            }
            return citas;
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public void delete(long id){
        try{
            Optional<Cita> cita = citaRepository.findById(id);
            if(cita.isEmpty()){
                throw new DataNotFoundException("Cita no encontrada");
            }else {
                citaRepository.deleteById(id);
            }
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
