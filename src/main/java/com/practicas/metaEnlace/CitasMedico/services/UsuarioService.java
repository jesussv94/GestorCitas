package com.practicas.metaEnlace.CitasMedico.services;

import com.practicas.metaEnlace.CitasMedico.dto.UsuarioDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Usuario;
import com.practicas.metaEnlace.CitasMedico.exceptions.DataNotFoundException;
import com.practicas.metaEnlace.CitasMedico.mapper.UsuarioMapper;
import com.practicas.metaEnlace.CitasMedico.repositories.UsuarioRepository;
import com.practicas.metaEnlace.CitasMedico.services.interfaces.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UsuarioService implements IUsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private UsuarioMapper usuarioMapper;


    public UsuarioDTO search(String usuario){
        try{
            Usuario buscarUsuario = usuarioRepository.findByUsuario(usuario).orElse(null);
            if(buscarUsuario == null){
                throw new DataNotFoundException("Usuario no encontrado");
            }
            return usuarioMapper.toUsuarioDTO(buscarUsuario);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
