package com.practicas.metaEnlace.CitasMedico.services;

import com.practicas.metaEnlace.CitasMedico.dto.PacienteDTO;
import com.practicas.metaEnlace.CitasMedico.dto.PacienteListDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Paciente;
import com.practicas.metaEnlace.CitasMedico.entities.Rol;
import com.practicas.metaEnlace.CitasMedico.entities.utils.Roles;
import com.practicas.metaEnlace.CitasMedico.exceptions.DataNotFoundException;
import com.practicas.metaEnlace.CitasMedico.exceptions.DuplicateDataException;
import com.practicas.metaEnlace.CitasMedico.mapper.PacienteMapper;
import com.practicas.metaEnlace.CitasMedico.repositories.CitaRepository;
import com.practicas.metaEnlace.CitasMedico.repositories.MedicoRepository;
import com.practicas.metaEnlace.CitasMedico.repositories.PacienteRepository;
import com.practicas.metaEnlace.CitasMedico.repositories.RolRepository;
import com.practicas.metaEnlace.CitasMedico.services.interfaces.IPacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PacienteService implements IPacienteService {
    @Autowired
    private PacienteRepository pacienteRepository;
    @Autowired
    PacienteMapper pacienteMapper;
    @Autowired
    private MedicoRepository medicoRepository;
    @Autowired
    private CitaRepository citaRepository;
    @Autowired
    private RolRepository rolRepository;


    public List<PacienteListDTO> list(){
        List<PacienteListDTO> lista = new ArrayList<>();
        Iterable<Paciente> listaPacientes = pacienteRepository.findAll();
        for(Paciente paciente : listaPacientes){
            PacienteListDTO pacienteListDTO = pacienteMapper.toPacienteListDTO(paciente);
            lista.add(pacienteListDTO);
        }
        return lista;
    }

    public Paciente insert(PacienteDTO pacienteDTO) {
        try{
            //Validaciones
            Optional<Paciente> pacienteUsuario = pacienteRepository.findByUsuario(pacienteDTO.getUsuario());
            Optional<Paciente> pacienteNss = pacienteRepository.findByNss(pacienteDTO.getNss());
            Optional<Paciente> pacienteTlf = pacienteRepository.findByTelefono(pacienteDTO.getTelefono());

            if(pacienteUsuario.isPresent()){
                throw new DuplicateDataException("Nombre de usuario ya existe");
            }
            if(pacienteNss.isPresent()){
                throw new DuplicateDataException("NSS duplicado");
            }
            if(pacienteTlf.isPresent()){
                throw new DuplicateDataException("Telefono duplicado");
            }
            //Codificado de la clave
            String pass = new BCryptPasswordEncoder().encode(pacienteDTO.getClave());
            //Rol
            Rol rol = new Rol();
            rol.setRol(Roles.PACIENTE);
            rolRepository.save(rol);

            Paciente paciente = pacienteMapper.toPaciente(pacienteDTO);
            paciente.setClave(pass);
            paciente.addRoles(rol);
            return pacienteRepository.save(paciente);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public Paciente update(Long id, PacienteDTO pacienteDTO){
        try{
            //Validaciones
            Optional<Paciente> pacienteUsuario = pacienteRepository.findByUsuario(pacienteDTO.getUsuario());
            Optional<Paciente> pacienteNss = pacienteRepository.findByNss(pacienteDTO.getNss());
            Optional<Paciente> pacienteTlf = pacienteRepository.findByTelefono(pacienteDTO.getTelefono());

            if(pacienteUsuario.isPresent() && id != pacienteUsuario.get().getId()){
                throw new DuplicateDataException("Nombre de usuario ya existe");
            }
            if(pacienteNss.isPresent() && id != pacienteNss.get().getId()){
                throw new DuplicateDataException("NSS duplicado");
            }
            if(pacienteTlf.isPresent() && id != pacienteTlf.get().getId()){
                throw new DuplicateDataException("Telefono duplicado");
            }
            //Codificado de la clave
            String pass = new BCryptPasswordEncoder().encode(pacienteDTO.getClave());

            Paciente paciente = pacienteMapper.toPaciente(pacienteDTO);
            paciente.setClave(pass);
            paciente.setId(id);
            return pacienteRepository.save(paciente);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public PacienteDTO searchId(Long id){
        try{
            Paciente paciente = pacienteRepository.findById(id).orElse(null);
            if(paciente == null){
                throw new DataNotFoundException("Paciente no encontrado");
            }
            return pacienteMapper.toPacienteDTO(paciente);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public PacienteDTO search(String usuario){
        try{
            Paciente paciente = pacienteRepository.findByUsuario(usuario).orElse(null);
            if(paciente == null){
                throw new DataNotFoundException("Paciente no encontrado");
            }
            return pacienteMapper.toPacienteDTO(paciente);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public void delete(Long id){
        try {
            Optional<Paciente> pacienteopt = pacienteRepository.findById(id);
            if(pacienteopt.isEmpty()){
                throw new DataNotFoundException("Paciente no encontrado");
            }else{
                pacienteRepository.deleteById(id);
            }
        }catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
