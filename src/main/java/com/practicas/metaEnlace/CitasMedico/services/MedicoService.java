package com.practicas.metaEnlace.CitasMedico.services;

import com.practicas.metaEnlace.CitasMedico.dto.MedicoDTO;
import com.practicas.metaEnlace.CitasMedico.dto.MedicoListDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Medico;
import com.practicas.metaEnlace.CitasMedico.entities.Rol;
import com.practicas.metaEnlace.CitasMedico.entities.utils.Roles;
import com.practicas.metaEnlace.CitasMedico.exceptions.DataNotFoundException;
import com.practicas.metaEnlace.CitasMedico.exceptions.DuplicateDataException;
import com.practicas.metaEnlace.CitasMedico.mapper.MedicoMapper;
import com.practicas.metaEnlace.CitasMedico.repositories.CitaRepository;
import com.practicas.metaEnlace.CitasMedico.repositories.MedicoRepository;
import com.practicas.metaEnlace.CitasMedico.repositories.PacienteRepository;
import com.practicas.metaEnlace.CitasMedico.repositories.RolRepository;
import com.practicas.metaEnlace.CitasMedico.services.interfaces.IMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MedicoService implements IMedicoService {
    @Autowired
    private MedicoRepository medicoRepository;
    @Autowired
    private MedicoMapper medicoMapper;
    @Autowired
    private PacienteRepository pacienteRepository;
    @Autowired
    private CitaRepository citaRepository;
    @Autowired
    private RolRepository rolRepository;


    public List<MedicoListDTO> list() {
        List<MedicoListDTO> lista = new ArrayList<>();
        Iterable<Medico> listaMedicos = medicoRepository.findAll();
        for(Medico medico : listaMedicos){
            MedicoListDTO medicoListDTO = medicoMapper.toMedicoListDTO(medico);
            lista.add(medicoListDTO);
        }
        return lista;
    }

    public Medico insert(MedicoDTO medicoDTO) {
        try{
            //Validaciones
            Optional<Medico> medicoUsuario = medicoRepository.findByUsuario(medicoDTO.getUsuario());
            if(medicoUsuario.isPresent()){
                throw new DuplicateDataException("Nombre de usuario ya existe");
            }
            Optional<Medico> numColegiado = medicoRepository.findByNumColegiado(medicoDTO.getNumColegiado());
            if(numColegiado.isPresent()){
                throw new DuplicateDataException("Numero de colegiado ya existe");
            }
            //Codificado de la clave
            String pass = new BCryptPasswordEncoder().encode(medicoDTO.getClave());
            //Rol
            Rol rol = new Rol();
            rol.setRol(Roles.MEDICO);
            rolRepository.save(rol);

            Medico medico = medicoMapper.toMedico(medicoDTO);
            medico.setClave(pass);
            medico.addRoles(rol);
            return medicoRepository.save(medico);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public Medico update(Long id, MedicoDTO medicoDTO){
        try{
            //Validaciones
            Optional<Medico> medicoUsuario = medicoRepository.findByUsuario(medicoDTO.getUsuario());
            Optional<Medico> numColegiado = medicoRepository.findByNumColegiado(medicoDTO.getNumColegiado());

            if(medicoUsuario.isPresent() && id != medicoUsuario.get().getId()){
                throw new DuplicateDataException("Nombre de usuario ya existe");
            }
            if(numColegiado.isPresent() && id != numColegiado.get().getId()){
                throw new DuplicateDataException("Numero de colegiado ya existe");
            }

            //Codificado de la clave
            String pass = new BCryptPasswordEncoder().encode(medicoDTO.getClave());

            Medico medico = medicoMapper.toMedico(medicoDTO);
            medico.setClave(pass);
            medico.setId(id);
            return medicoRepository.save(medico);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public MedicoDTO searchId(long id){
        try{
            Medico medico = medicoRepository.findById(id).orElse(null);
            if(medico == null){
                throw new DataNotFoundException("Médico no encontrado");
            }
            return medicoMapper.toMedicoDTO(medico);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public MedicoDTO search(String usuario) {
        try{
            Medico medico = medicoRepository.findByUsuario(usuario).orElse(null);
            if(medico == null){
                throw new DataNotFoundException("Médico no encontrado");
            }
            return medicoMapper.toMedicoDTO(medico);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public void delete(Long id){
        try{
            Optional<Medico> medicoOp = medicoRepository.findById(id);
            if(!medicoOp.isPresent()){
                throw new DataNotFoundException("Médico no encontrado");
            }else{
                medicoRepository.deleteById(id);
            }
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
