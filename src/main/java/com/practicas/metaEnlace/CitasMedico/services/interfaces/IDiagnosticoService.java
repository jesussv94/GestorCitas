package com.practicas.metaEnlace.CitasMedico.services.interfaces;

import com.practicas.metaEnlace.CitasMedico.dto.DiagnosticoDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Diagnostico;

import java.util.List;

public interface IDiagnosticoService {
    public List<DiagnosticoDTO> list();
    public Diagnostico update(Long id, DiagnosticoDTO diagnosticoDTO);
    public DiagnosticoDTO search(long id);
    public void delete(long id);
}
