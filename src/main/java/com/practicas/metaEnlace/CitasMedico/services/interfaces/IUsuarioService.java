package com.practicas.metaEnlace.CitasMedico.services.interfaces;

import com.practicas.metaEnlace.CitasMedico.dto.UsuarioDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Usuario;

public interface IUsuarioService {
    public UsuarioDTO search(String usuario);
}
