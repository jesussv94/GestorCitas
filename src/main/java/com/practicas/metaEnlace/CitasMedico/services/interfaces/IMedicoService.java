package com.practicas.metaEnlace.CitasMedico.services.interfaces;

import com.practicas.metaEnlace.CitasMedico.dto.MedicoDTO;
import com.practicas.metaEnlace.CitasMedico.dto.MedicoListDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Medico;

import java.util.List;

public interface IMedicoService {
    public List<MedicoListDTO> list();
    public Medico insert(MedicoDTO medicoDTO);
    public Medico update(Long id, MedicoDTO medicoDTO);
    public MedicoDTO searchId(long id);
    public MedicoDTO search(String usuario);
    public void delete(Long id);
}
