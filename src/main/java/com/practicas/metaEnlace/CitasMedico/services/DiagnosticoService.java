package com.practicas.metaEnlace.CitasMedico.services;

import com.practicas.metaEnlace.CitasMedico.dto.DiagnosticoDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Cita;
import com.practicas.metaEnlace.CitasMedico.entities.Diagnostico;
import com.practicas.metaEnlace.CitasMedico.exceptions.DataNotFoundException;
import com.practicas.metaEnlace.CitasMedico.mapper.DiagnosticoMapper;
import com.practicas.metaEnlace.CitasMedico.repositories.CitaRepository;
import com.practicas.metaEnlace.CitasMedico.repositories.DiagnosticoRepository;
import com.practicas.metaEnlace.CitasMedico.services.interfaces.IDiagnosticoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DiagnosticoService implements IDiagnosticoService {
    @Autowired
    private DiagnosticoRepository diagnosticoRepository;
    @Autowired
    private DiagnosticoMapper diagnosticoMapper;
    @Autowired
    private CitaRepository citaRepository;

    public Diagnostico update(Long id, DiagnosticoDTO diagnosticoDTO){
        try{
            Optional<Cita> diagnosticoId = citaRepository.findByDiagnosticoId(id);
            if(!diagnosticoId.isPresent()){
                throw new DataNotFoundException("Diagnostico no encontrado");
            }
            Diagnostico diagnostico = diagnosticoMapper.toDiagnostico(diagnosticoDTO);
            diagnostico.setId(id);
            return diagnosticoRepository.save(diagnostico);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public DiagnosticoDTO search(long id){
        try{
            Diagnostico diagnostico = diagnosticoRepository.findById(id).orElse(null);
            if(diagnostico == null){
                throw new DataNotFoundException("Diagnóstico no encontrado");
            }
            return diagnosticoMapper.toDiagnosticoDTO(diagnostico);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    public List<DiagnosticoDTO> list(){
        List<DiagnosticoDTO> lista = new ArrayList<>();
        Iterable<Diagnostico> listaDiag = diagnosticoRepository.findAll();
        for(Diagnostico diagnostico : listaDiag){
            DiagnosticoDTO diagnosticoDTO = diagnosticoMapper.toDiagnosticoDTO(diagnostico);
            lista.add(diagnosticoDTO);
        }
        return lista;
    }

    public void delete(long id){
        try{
            Optional<Diagnostico> diagnostico = diagnosticoRepository.findById(id);
            if(diagnostico.isEmpty()){
                throw new DataNotFoundException("Diagnostico no encontrado");
            } else {
                diagnosticoRepository.deleteById(id);
            }
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
