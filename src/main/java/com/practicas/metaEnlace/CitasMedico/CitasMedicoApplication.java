package com.practicas.metaEnlace.CitasMedico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitasMedicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CitasMedicoApplication.class, args);
	}

}
