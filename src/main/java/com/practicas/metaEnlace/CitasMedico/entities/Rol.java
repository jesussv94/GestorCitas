package com.practicas.metaEnlace.CitasMedico.entities;

import com.practicas.metaEnlace.CitasMedico.entities.utils.Roles;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(name = "Rol")
public class Rol {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    @Enumerated(EnumType.STRING)
    private Roles rol;
    @ManyToOne
    private Usuario usuario;
}
