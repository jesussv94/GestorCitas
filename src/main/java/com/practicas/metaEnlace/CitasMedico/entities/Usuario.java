package com.practicas.metaEnlace.CitasMedico.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Usuario")
@Inheritance(strategy=InheritanceType.JOINED)
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellidos")
    private String apellidos;
    @Column(name = "usuario", unique = true)
    private String usuario;
    @Column(name = "clave")
    private String clave;

    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "usuario_id")
    private List<Rol> roles;

    public void addRoles(Rol rol){
        if(roles == null){
            roles = new ArrayList<>();
        }
        if(!roles.contains(rol)){
            roles.add(rol);
        }
    }
}
