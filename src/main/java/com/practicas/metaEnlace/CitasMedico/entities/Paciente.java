package com.practicas.metaEnlace.CitasMedico.entities;

import com.practicas.metaEnlace.CitasMedico.dto.CitaDTO;
import com.practicas.metaEnlace.CitasMedico.dto.MedicoDTO;
import com.practicas.metaEnlace.CitasMedico.exceptions.DuplicateDataException;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Paciente")
public class Paciente extends Usuario {
    @Column(name= "nss", unique = true)
    private String nss;
    @Column(name = "numTarjeta")
    private String numTarjeta;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "direccion")
    private String direccion;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "RelacionMedicoPaciente",
            joinColumns = { @JoinColumn(name = "paciente_id")},
            inverseJoinColumns = { @JoinColumn(name = "medico_id")}
    )
    private List<Medico> medicos;

    @OneToMany(mappedBy="paciente", cascade = CascadeType.REMOVE)
    private List<Cita> citas;

    public void addCita(Cita cita){
        if(citas == null){
            citas = new ArrayList<>();
        }
        citas.add(cita);
    }
    public void addMedico(Medico medico){
        if(medicos == null){
            medicos = new ArrayList<>();
        }
        if(!medicos.contains(medico)){
            medicos.add(medico);
        }
    }
}
