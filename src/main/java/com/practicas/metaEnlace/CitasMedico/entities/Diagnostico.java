package com.practicas.metaEnlace.CitasMedico.entities;

import jakarta.persistence.*;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Diagnostico")
@Table(name = "Diagnostico")
public class Diagnostico {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    @Column(name = "valoracionEspecialista")
    private String valoracionEspecialista;
    @Column(name = "enfermedad")
    private String enfermedad;

    @OneToOne(mappedBy = "diagnostico")
    private Cita cita;
}
