package com.practicas.metaEnlace.CitasMedico.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Medico")
public class Medico extends Usuario{
    @Column(name = "numColegiado", unique = true)
    private String numColegiado;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "RelacionMedicoPaciente",
            joinColumns = { @JoinColumn(name = "medico_id")},
            inverseJoinColumns = { @JoinColumn(name = "paciente_id")}
    )
    private List<Paciente> pacientes;

    @OneToMany(mappedBy="medico", cascade = CascadeType.REMOVE)
    private List<Cita> citas;

    public void addCita(Cita cita){
        if(citas == null){
            citas = new ArrayList<>();
        }
        citas.add(cita);
    }
    public void addPaciente(Paciente paciente){
        if(pacientes == null){
            pacientes = new ArrayList<>();
        }
        pacientes.add(paciente);
    }
}
