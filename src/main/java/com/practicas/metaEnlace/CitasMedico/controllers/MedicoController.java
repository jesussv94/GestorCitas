package com.practicas.metaEnlace.CitasMedico.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.practicas.metaEnlace.CitasMedico.Encryption.JceEncryptor;
import com.practicas.metaEnlace.CitasMedico.dto.MedicoDTO;
import com.practicas.metaEnlace.CitasMedico.dto.MedicoListDTO;
import com.practicas.metaEnlace.CitasMedico.services.MedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.SecretKey;
import java.util.List;

@RestController
@RequestMapping("api/medico")
public class MedicoController {
    @Autowired
    private MedicoService medicoService;


    @GetMapping("/list")
    public ResponseEntity<List<MedicoListDTO>> list() {
        List<MedicoListDTO> medicos = medicoService.list();
        return ResponseEntity.ok(medicos);
    }

    @GetMapping("/listaEncriptada")
    public ResponseEntity<byte[]> listaEncriptada() throws Exception {
        List<MedicoListDTO> medicos = medicoService.list();
        //JSON a string
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonSinCifrar = objectMapper.writeValueAsString(medicos);

        byte[] keyData = "claveSecreta1234".getBytes();
        SecretKey key = JceEncryptor.generateKey(keyData);
        // Cifrar el JSON
        byte[] encryptedData = JceEncryptor.encrypt(jsonSinCifrar, key);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Content-Disposition", "attachment; filename=medicos.json");
        headers.add("Access-Control-Expose-Headers", "Content-Disposition");
        return new ResponseEntity<>(encryptedData, headers, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity insert(@RequestBody MedicoDTO medicoDTO){
        medicoService.insert(medicoDTO);
        return ResponseEntity.ok("200");
    }

    @PutMapping("/update/{id}")
    ResponseEntity update(@RequestBody MedicoDTO medicoDTO, @PathVariable Long id){
        medicoService.update(id, medicoDTO);
        return ResponseEntity.ok("200");
    }

    @GetMapping("/search/{id}")
    public ResponseEntity<MedicoDTO> searchId(@PathVariable long id){
        MedicoDTO medico = medicoService.searchId(id);
        return ResponseEntity.ok(medico);
    }

    @GetMapping("/searchEncrypt/{id}")
    public ResponseEntity<byte[]> buscarIdEncriptada(@PathVariable long id) throws Exception {
        MedicoDTO medico = medicoService.searchId(id);

        byte[] keyData = "claveSecreta1234".getBytes();
        SecretKey key = JceEncryptor.generateKey(keyData);
        // Cifrar el JSON
        byte[] encryptedData = JceEncryptor.encrypt(medico.toString(), key);

        return ResponseEntity.ok(encryptedData);
    }

    @GetMapping("/{usuario}")
    public ResponseEntity<MedicoDTO> search(@PathVariable String usuario){
        MedicoDTO medico = medicoService.search(usuario);
        return ResponseEntity.ok(medico);
    }

    @DeleteMapping("/delete/{id}")
    ResponseEntity delete(@PathVariable Long id){
        medicoService.delete(id);
        return ResponseEntity.ok("200");
    }

}
