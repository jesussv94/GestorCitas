package com.practicas.metaEnlace.CitasMedico.controllers;

import com.practicas.metaEnlace.CitasMedico.dto.DiagnosticoDTO;
import com.practicas.metaEnlace.CitasMedico.services.DiagnosticoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/diagnostico")
public class DiagnosticoController {
    @Autowired
    private DiagnosticoService diagnosticoService;

    @GetMapping("/list")
    public ResponseEntity<List<DiagnosticoDTO>> list(){
        List<DiagnosticoDTO> diagnosticos = diagnosticoService.list();
        return ResponseEntity.ok(diagnosticos);
    }

    @PutMapping("update/{id}")
    public ResponseEntity update(@PathVariable Long id, @RequestBody DiagnosticoDTO diagnosticoDTO){
        diagnosticoService.update(id, diagnosticoDTO);
        return ResponseEntity.ok("200");
    }

    @GetMapping("search/{id}")
    public ResponseEntity search(@PathVariable Long id){
        DiagnosticoDTO diagnosticoDTO = diagnosticoService.search(id);
        return ResponseEntity.ok(diagnosticoDTO);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        diagnosticoService.delete(id);
        return ResponseEntity.ok("200");
    }
}
