package com.practicas.metaEnlace.CitasMedico.controllers;

import com.practicas.metaEnlace.CitasMedico.dto.PacienteDTO;
import com.practicas.metaEnlace.CitasMedico.dto.PacienteListDTO;
import com.practicas.metaEnlace.CitasMedico.services.PacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/paciente")
public class PacienteController {
    @Autowired
    private PacienteService pacienteService;


    @GetMapping("/list")
    public ResponseEntity<List<PacienteListDTO>> list(){
        List<PacienteListDTO> pacientes = pacienteService.list();
        return ResponseEntity.ok(pacientes);
    }

    @PostMapping
    public ResponseEntity insert(@RequestBody PacienteDTO pacienteDTO){
        pacienteService.insert(pacienteDTO);
        return ResponseEntity.ok("200");
    }

    @PutMapping("/update/{id}")
    public ResponseEntity update(@RequestBody PacienteDTO pacienteDTO, @PathVariable Long id){
        pacienteService.update(id, pacienteDTO);
        return ResponseEntity.ok("200");
    }

    @GetMapping("/search/{id}")
    public ResponseEntity<PacienteDTO> searchId(@PathVariable long id){
        PacienteDTO paciente = pacienteService.searchId(id);
        return ResponseEntity.ok(paciente);
    }

    @GetMapping("/{usuario}")
    public ResponseEntity search(@PathVariable String usuario){
        PacienteDTO pacienteDTO = pacienteService.search(usuario);
        return ResponseEntity.ok(pacienteDTO);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        pacienteService.delete(id);
        return ResponseEntity.ok("200");
    }

}
