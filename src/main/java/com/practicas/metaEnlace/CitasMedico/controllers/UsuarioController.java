package com.practicas.metaEnlace.CitasMedico.controllers;

import com.practicas.metaEnlace.CitasMedico.dto.UsuarioDTO;
import com.practicas.metaEnlace.CitasMedico.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/usuario")
public class UsuarioController {
    @Autowired
    UsuarioService usuarioService;

    @GetMapping("/search/{usuario}")
    public ResponseEntity<UsuarioDTO> search(@PathVariable String usuario){
        UsuarioDTO usuarioDTO = usuarioService.search(usuario);
        return ResponseEntity.ok(usuarioDTO);
    }
}
