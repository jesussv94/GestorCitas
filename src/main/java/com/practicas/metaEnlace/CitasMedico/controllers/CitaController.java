package com.practicas.metaEnlace.CitasMedico.controllers;

import com.practicas.metaEnlace.CitasMedico.dto.CitaDTO;
import com.practicas.metaEnlace.CitasMedico.dto.CitaListDTO;
import com.practicas.metaEnlace.CitasMedico.services.CitaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/cita")
public class CitaController {
    @Autowired
    private CitaService citaService;


    @GetMapping("/list")
    public ResponseEntity<List<CitaListDTO>> list(){
        List<CitaListDTO> citas = citaService.list();
        return ResponseEntity.ok(citas);
    }

    @PostMapping
    public ResponseEntity insert(@RequestBody CitaDTO citaDTO) {
        citaService.insert(citaDTO);
        return ResponseEntity.ok("200");
    }

    @PutMapping("update/{id}")
    public ResponseEntity update(@RequestBody CitaDTO citaDTO, @PathVariable Long id){
        citaService.update(id, citaDTO);
        return ResponseEntity.ok("200");
    }

    @GetMapping("search/{id}")
    public ResponseEntity search(@PathVariable Long id){
        CitaListDTO citaListDTO = citaService.search(id);
        return ResponseEntity.ok(citaListDTO);
    }

    @GetMapping("searchByPacienteId/{id}")
    public ResponseEntity<List<CitaListDTO>> searchByPacienteId(@PathVariable Long id){
        List<CitaListDTO> citas = citaService.searchByPacienteId(id);
        return ResponseEntity.ok(citas);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        citaService.delete(id);
        return ResponseEntity.ok("200");
    }
}
