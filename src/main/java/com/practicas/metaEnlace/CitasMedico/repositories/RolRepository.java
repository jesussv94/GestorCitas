package com.practicas.metaEnlace.CitasMedico.repositories;

import com.practicas.metaEnlace.CitasMedico.entities.Rol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RolRepository extends JpaRepository<Rol, Long> {
    Optional<Rol> findByRol(String rol);
}
