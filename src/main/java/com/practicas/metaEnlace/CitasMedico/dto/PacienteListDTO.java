package com.practicas.metaEnlace.CitasMedico.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PacienteListDTO {
    private long id;
    private String nombre;
    private String apellidos;
    private String usuario;
    private String nss;
    private String numTarjeta;
    private String telefono;
    private String direccion;

}
