package com.practicas.metaEnlace.CitasMedico.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class UsuarioDTO {
    private long id;
    private String nombre;
    private String apellidos;
    private String usuario;
    private List<RolDTO> roles;
}
