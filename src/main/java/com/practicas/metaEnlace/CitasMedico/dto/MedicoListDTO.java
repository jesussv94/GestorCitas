package com.practicas.metaEnlace.CitasMedico.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MedicoListDTO {
    private long id;
    private String nombre;
    private String apellidos;
    private String usuario;
    private String numColegiado;
}
