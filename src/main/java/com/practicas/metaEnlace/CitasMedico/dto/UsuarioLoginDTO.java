package com.practicas.metaEnlace.CitasMedico.dto;

import com.practicas.metaEnlace.CitasMedico.entities.Rol;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class UsuarioLoginDTO {
    private String usuario;
    private String clave;
    private List<Rol> roles;
}
