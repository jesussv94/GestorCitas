package com.practicas.metaEnlace.CitasMedico.dto;

import com.practicas.metaEnlace.CitasMedico.entities.Diagnostico;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CitaListDTO {
    private long id;
    private Date fechaHora;
    private String motivoCita;
    private int attribute11;
    private long pacienteId;
    private long medicoId;
    private long diagnosticoId;
}
