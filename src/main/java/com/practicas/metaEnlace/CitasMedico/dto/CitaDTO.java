package com.practicas.metaEnlace.CitasMedico.dto;

import com.practicas.metaEnlace.CitasMedico.entities.Diagnostico;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CitaDTO {
    private long id;
    private Date fechaHora;
    private String motivoCita;
    private int attribute11;
    private long pacienteId;
    private long medicoId;
    private Diagnostico diagnostico;
}
