package com.practicas.metaEnlace.CitasMedico.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiagnosticoDTO {
    private long id;
    private String valoracionEspecialista;
    private String enfermedad;
}
