package com.practicas.metaEnlace.CitasMedico.dto;

import com.practicas.metaEnlace.CitasMedico.entities.utils.Roles;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RolDTO {
    private long id;
    private Roles rol;
}
