package com.practicas.metaEnlace.CitasMedico.mapper;

import com.practicas.metaEnlace.CitasMedico.dto.CitaDTO;
import com.practicas.metaEnlace.CitasMedico.dto.CitaListDTO;
import com.practicas.metaEnlace.CitasMedico.entities.Cita;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CitaMapper {
    CitaMapper INSTANCE = Mappers.getMapper(CitaMapper.class);
    @Mapping(target = "paciente.id", source = "citaDTO.pacienteId")
    @Mapping(target = "medico.id", source = "citaDTO.medicoId")
    Cita toCita(CitaDTO citaDTO);

    @Mapping(target = "pacienteId", source = "cita.paciente.id")
    @Mapping(target = "medicoId", source = "cita.medico.id")
    CitaDTO toCitaDTO(Cita cita);

    @Mapping(target = "pacienteId", source = "cita.paciente.id")
    @Mapping(target = "medicoId", source = "cita.medico.id")
    @Mapping(target = "diagnosticoId", source = "cita.diagnostico.id")
    CitaListDTO toCitaListDTO(Cita cita);
}
