package com.practicas.metaEnlace.CitasMedico.security.utils;

import com.practicas.metaEnlace.CitasMedico.entities.Rol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

public class SecurityRol implements GrantedAuthority {
    @Autowired
    private Rol rol;

    @Override
    public String getAuthority() {
        return rol.getRol().toString();
    }
}
