package com.practicas.metaEnlace.CitasMedico.security.utils;

import com.practicas.metaEnlace.CitasMedico.entities.Usuario;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

@AllArgsConstructor
public class SecurityUser implements UserDetails {
    private final Usuario usuario;


    @Override
    public String getUsername() {
        return usuario.getUsuario();
    }

    @Override
    public String getPassword() {
        return usuario.getClave();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
        //return usuario.getRoles().stream().map(SecurityRol::new).toList();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
