package com.practicas.metaEnlace.CitasMedico.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.practicas.metaEnlace.CitasMedico.entities.Usuario;
import com.practicas.metaEnlace.CitasMedico.security.utils.SecurityUser;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Collections;

public class JWT_AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
        throws AuthenticationException{
        Usuario usuario = new Usuario();
        try{
            usuario = new ObjectMapper().readValue(request.getReader(), Usuario.class);
        }catch (IOException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                usuario.getUsuario(),
                usuario.getClave(),
                Collections.emptyList());
                //usuario.getRoles().stream().map(SecurityRol::new).toList());
        return getAuthenticationManager().authenticate(auth);
    }
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult)
                                            throws IOException, ServletException {

        SecurityUser userDetails = (SecurityUser) authResult.getPrincipal();
        String token = JWT_Util.generarToken(userDetails.getUsername());

        response.addHeader("Authorization", "Bearer" + token);
        response.getWriter().flush();

        super.successfulAuthentication(request, response, chain, authResult);
    }
}

