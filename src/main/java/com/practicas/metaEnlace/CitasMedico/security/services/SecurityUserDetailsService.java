package com.practicas.metaEnlace.CitasMedico.security.services;

import com.practicas.metaEnlace.CitasMedico.entities.Usuario;
import com.practicas.metaEnlace.CitasMedico.repositories.UsuarioRepository;
import com.practicas.metaEnlace.CitasMedico.security.utils.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SecurityUserDetailsService implements UserDetailsService {
    @Autowired
    private UsuarioRepository usuarioRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<Usuario> usuarioOpt = usuarioRepository.findByUsuario(username);

        if(usuarioOpt.isPresent()){
            Usuario usuario = usuarioOpt.get();
            return new SecurityUser(usuario);
        }
        throw new UsernameNotFoundException("Usuario no encontrado");
    }

}
